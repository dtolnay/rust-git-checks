// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Module for importing all types and traits for implementing checks.

pub use std::error::Error;

pub use git_workarea::CommitId;
pub use git_workarea::GitError;
pub use git_workarea::Identity;

pub use crate::check::BranchCheck;
pub use crate::check::Check;
pub use crate::check::CheckResult;
pub use crate::check::ContentCheck;
pub use crate::check::TopicCheck;
pub use crate::commit::Commit;
pub use crate::commit::CommitError;
pub use crate::commit::Content;
pub use crate::commit::DiffInfo;
pub use crate::commit::FileName;
pub use crate::commit::StatusChange;
pub use crate::commit::Topic;
pub use crate::context::AttributeState;
pub use crate::context::CheckGitContext;
pub use crate::utils::SubmoduleContext;

/// Create a prefix to a message referencing the associated.
pub fn commit_prefix(content: &dyn Content) -> String {
    if let Some(sha1) = content.sha1() {
        format!("commit {} ", sha1)
    } else {
        String::new()
    }
}

/// Create a prefix to a message referencing the associated with a description string.
pub fn commit_prefix_str<M>(content: &dyn Content, msg: M) -> String
where
    M: AsRef<str>,
{
    if let Some(sha1) = content.sha1() {
        format!("commit {} {} ", sha1, msg.as_ref())
    } else {
        String::new()
    }
}
