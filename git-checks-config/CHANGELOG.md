# v0.2.0 (unreleased)

  * MSRV bumped to 1.45. While the code update is just for a small code
    simplification, in order to simplify CI, updating makes sense.

# v0.1.0

  * Initial release.
