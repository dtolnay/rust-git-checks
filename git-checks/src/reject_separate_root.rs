// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use derive_builder::Builder;
use git_checks_core::impl_prelude::*;

/// A check which denies root commits.
#[derive(Builder, Debug, Default, Clone, Copy)]
#[builder(field(private))]
pub struct RejectSeparateRoot {}

impl RejectSeparateRoot {
    /// Create a new builder.
    pub fn builder() -> RejectSeparateRootBuilder {
        RejectSeparateRootBuilder::default()
    }
}

impl Check for RejectSeparateRoot {
    fn name(&self) -> &str {
        "reject-separate-root"
    }

    fn check(&self, _: &CheckGitContext, commit: &Commit) -> Result<CheckResult, Box<dyn Error>> {
        let mut result = CheckResult::new();

        if commit.parents.is_empty() {
            result.add_error(format!(
                "commit {} not allowed; it is a root commit.",
                commit.sha1,
            ));
        }

        Ok(result)
    }
}

#[cfg(feature = "config")]
pub(crate) mod config {
    use git_checks_config::{register_checks, CommitCheckConfig, IntoCheck};
    use serde::Deserialize;
    #[cfg(test)]
    use serde_json::json;

    use crate::RejectSeparateRoot;

    /// Configuration for the `RejectSeparateRoot` check.
    ///
    /// No configuration available.
    ///
    /// This check is registered as a commit check with the name `"reject_separate_root"`.
    #[derive(Deserialize, Debug)]
    pub struct RejectSeparateRootConfig {}

    impl IntoCheck for RejectSeparateRootConfig {
        type Check = RejectSeparateRoot;

        fn into_check(self) -> Self::Check {
            RejectSeparateRoot::default()
        }
    }

    register_checks! {
        RejectSeparateRootConfig {
            "reject_separate_root" => CommitCheckConfig,
        },
    }

    #[test]
    fn test_reject_separate_root_config_empty() {
        let json = json!({});
        let check: RejectSeparateRootConfig = serde_json::from_value(json).unwrap();

        let _ = check.into_check();
    }
}

#[cfg(test)]
mod tests {
    use git_checks_core::Check;

    use crate::test::*;
    use crate::RejectSeparateRoot;

    const NO_ROOT_TOPIC: &str = "ba3dc3cb09a558c88282742413a2dccb17d444fc";
    const WITH_ROOT_TOPIC: &str = "ff560e8798ef7a9d10bf43660695f7155b49b398";

    #[test]
    fn test_reject_separate_root_builder_default() {
        assert!(RejectSeparateRoot::builder().build().is_ok());
    }

    #[test]
    fn test_reject_separate_root_name_commit() {
        let check = RejectSeparateRoot::default();
        assert_eq!(Check::name(&check), "reject-separate-root");
    }

    #[test]
    fn test_reject_separate_root_no_root() {
        let check = RejectSeparateRoot::default();
        run_check_ok("test_reject_separate_root_no_root", NO_ROOT_TOPIC, check);
    }

    #[test]
    fn test_reject_separate_root_with_root() {
        let check = RejectSeparateRoot::default();
        let result = run_check(
            "test_reject_separate_root_with_root",
            WITH_ROOT_TOPIC,
            check,
        );
        test_result_errors(
            result,
            &["commit ff560e8798ef7a9d10bf43660695f7155b49b398 not allowed; it is a root commit."],
        );
    }
}
